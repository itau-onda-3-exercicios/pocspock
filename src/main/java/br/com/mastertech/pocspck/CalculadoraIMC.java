package br.com.mastertech.pocspck;

public class CalculadoraIMC {

	public double CalcularIMC(double peso, double altura) {
		return Math.round(peso / (altura*altura)) ;
	}
	
	public String CalcularSituacaoSaude(double peso, double altura) {
		double imc = CalcularIMC(peso, altura);
		
		if (imc < 18.5) {
			return "Você está abaixo do peso ideal";
		} else if (imc <= 24.9) {
			return "Parabéns – você está em seu peso ideal";
		} else if (imc <= 29.9) {
			return "Você está acima do seu peso";
		} else if (imc <= 34.9) {
			return "Obesidade grau I";
		} else if (imc <= 39.9) {
			return "Obesidade grau II";
		} else {
			return "Obesidade grau III";
		}
	}
}
