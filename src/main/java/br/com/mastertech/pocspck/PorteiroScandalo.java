package br.com.mastertech.pocspck;

public class PorteiroScandalo {
	
	public boolean podeEntrar(int idade) {
		return idade >= 18;
	}
	
	public double getDesconto(int idade) {
		if (idade <= 25) {
			return 0;
		} else if (idade <= 40) {
			return 10;
		} else {
			return 20;
		}
	}
}
