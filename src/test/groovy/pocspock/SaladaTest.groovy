package pocspock

import spock.lang.Specification

class SaladaTest extends Specification {
	
	def 'garanta que a salada possua 4 ou mais frutas'(){
		given: 'montando a salada com os pacotes'
		def pacote1 = ['abacaxi', 'maça']
		def pacote2 = ['uva']
		//def pacote2 = ['uva', 'laranja']
		
		when: 'preparando a salada'
		def salada = pacote1 + pacote2
		
		then: 'a salada deve ter pelo menos 4 frutas'
		salada.size() >= 4
	}
	
}
