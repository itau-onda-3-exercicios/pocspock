package pocspock

import br.com.mastertech.pocspck.CalculadoraIMC
import spock.lang.Specification

class CalculadoraIMCTest extends Specification {
	
	def 'deve calcular o IMC'() {
		expect: 'O IMC deve ser calculado conforme o peso e a altura'
		new CalculadoraIMC().CalcularIMC(peso, altura) == imc
		
		where:
		peso  | altura | imc
		60	  | 1.7    | 21
		60    | 1.9    | 17
		120   | 1.8    | 37

	}
	
	def 'deve calcular a situacao de saude'() {
		expect: 'A situação de saúde de ser conforme o peso e a altura'
		new CalculadoraIMC().CalcularSituacaoSaude(peso, altura) == situacao
		
		where:
		peso  | altura | situacao
		60    | 1.9    | 'Você está abaixo do peso ideal'
		60    | 1.7    | 'Parabéns – você está em seu peso ideal'
		120   | 1.8    | 'Obesidade grau II'
		

	}
}
