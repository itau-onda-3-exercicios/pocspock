package pocspock

import spock.lang.Specification

class MusicasTest extends Specification {
	
	def 'deve cantar a música certa'(){
		given: 'receber o nome da moça'
		def mina = 'Jenifer'
		
		when: 'cante a música'
		def musica = "o nome dela é $mina"
		
		then: 'a música deve ser sobre a Jenifer'
		musica == "o nome dela é Jenifer"
	}
}
