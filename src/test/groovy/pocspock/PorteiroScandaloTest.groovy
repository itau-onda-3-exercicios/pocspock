package pocspock

import br.com.mastertech.pocspck.PorteiroScandalo
import spock.lang.Specification

class PorteiroScandaloTest extends Specification {
	
	def 'deve liberar caso seja maior de idade'() {
		given: 'receber a idade do cliente'
		def idade1 = 21
		def idade2 = 17
		
		when: 'verificar a idade'
		def porteiro = new PorteiroScandalo()
		def verificacao1 = porteiro.podeEntrar(idade1)
		def verificacao2 = porteiro.podeEntrar(idade2)
		
		then: 'deve liberar só para idade a partir de 18'
		verificacao1
		!verificacao2
	}
	
	def 'deve calcular o desconto conforme a idade'() {
		expect: '0 desconto deve estar correto conforme a idade'
		new PorteiroScandalo().getDesconto(idade) == desconto
		
		where: 
		idade | desconto
		18	  | 0
		23    | 0
		25    | 10
		26    | 10
		39    | 10
		42    | 20
		50    | 30

	}
}

